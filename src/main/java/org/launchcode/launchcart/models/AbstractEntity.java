package org.launchcode.launchcart.models;

import org.springframework.hateoas.ResourceSupport;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created by LaunchCode
 */
@MappedSuperclass
public class AbstractEntity extends ResourceSupport {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @XmlAttribute
    private int uid;

    public int getUid() {
        return uid;
    }
}
