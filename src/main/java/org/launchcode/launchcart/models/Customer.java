package org.launchcode.launchcart.models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * Created by LaunchCode
 */
@Entity
public class Customer extends User {

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cart_uid")
    private Cart cart;

    public Customer() {}

    public Customer(String username, String password) {
        super(username, password);
        cart = new Cart();
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }
}
