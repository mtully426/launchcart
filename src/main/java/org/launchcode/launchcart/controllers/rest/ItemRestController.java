package org.launchcode.launchcart.controllers.rest;

import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by LaunchCode
 */
@RestController
@RequestMapping("/api/items")
public class ItemRestController {

    @Autowired
    ItemRepository itemRepository;

    @GetMapping
    public List<Item> getItems(@RequestParam(required = false) Double price, @RequestParam(required = false) Boolean newItem) {
        List<Item> items;
        if( price != null && newItem != null){
            items = itemRepository.findByPriceAndNewItem(price, newItem);
        }
        else if (price != null) {
            items = itemRepository.findByPrice(price);
        } else if (newItem != null) {
            items =  itemRepository.findByNewItem(newItem);
        } else {
            items =  itemRepository.findAll();
        }
        for(Item item:items){
            item.add(linkTo(methodOn(ItemRestController.class).getSingleItem(item.getUid())).withSelfRel());
        }
        return items;
    }

    @GetMapping("/{id}")
    public Item getSingleItem(@PathVariable("id") int id) {
        Item item = itemRepository.findOne(id);
        item.add(linkTo(methodOn(ItemRestController.class).getSingleItem(id)).withSelfRel());
        return item;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Item addItem(@RequestBody Item item) {
        return itemRepository.save(item);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity updateItem(@PathVariable("id") int id, @RequestBody Item item) {
        if (item.getUid() != id) {
            return new ResponseEntity("No item found for ID" + id, HttpStatus.NOT_FOUND);
        }
        itemRepository.save(item);
        return new ResponseEntity(item, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteItem(@PathVariable("id") int id) {
        Item itemToDelete = itemRepository.findOne(id);
        if (itemToDelete == null) {
            return new ResponseEntity("No item found for ID" + id, HttpStatus.NOT_FOUND);
        }
        itemRepository.delete(id);
        return new ResponseEntity(id, HttpStatus.OK);
    }


}
