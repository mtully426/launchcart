package org.launchcode.launchcart.controllers.rest;

import org.launchcode.launchcart.data.CartRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/carts")
public class CartRestController {

    @Autowired
    private CartRepository cartRepository;

    @GetMapping
    public List<Cart> getAllCarts(){
        List<Cart> carts = cartRepository.findAll();
        for(Cart cart : carts){
            cart.add(linkTo(methodOn(CartRestController.class).getSingleCart(cart.getUid())).withSelfRel());
            for(Item item : cart.getItems()){
                item.add(linkTo(methodOn(ItemRestController.class).getSingleItem(item.getUid())).withRel(item.getName()));

            }
        }
        return carts;
    }

    @GetMapping(value = "/{id}")
    public Cart getSingleCart(@PathVariable("id") int id) {
        Cart cart = cartRepository.findOne(id);
        cart.add(linkTo(methodOn(CartRestController.class).getSingleCart(cart.getUid())).withSelfRel());
        for(Item item:cart.getItems()){
            item.add(linkTo(methodOn(ItemRestController.class).getSingleItem(item.getUid())).withRel(item.getName()));
        }
        return cart;
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity updateCart(@PathVariable("id") int id, @RequestBody Cart cart) {
        if (cart.getUid() != id) {
            return new ResponseEntity("No cart found for ID" + id, HttpStatus.NOT_FOUND);
        }
        cartRepository.save(cart);
        return new ResponseEntity(cart, HttpStatus.OK);
    }
}
