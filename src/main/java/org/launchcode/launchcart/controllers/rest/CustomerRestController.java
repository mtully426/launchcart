package org.launchcode.launchcart.controllers.rest;

import org.launchcode.launchcart.data.CartRepository;
import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Customer;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/api/customers")
public class CustomerRestController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CartRepository cartRepository;

    @GetMapping
    public List<Customer> getCustomers(){
        List<Customer> customers =  customerRepository.findAll();
        for(Customer customer :customers){
            customer.add(linkTo(methodOn(CustomerRestController.class).getSingleCustomer(customer.getUid())).withSelfRel());
            customer.add(linkTo(methodOn(CustomerRestController.class).getCustomerCart(customer.getUid())).withRel("cart"));
            for(Item item: customer.getCart().getItems()){
                item.add(linkTo(methodOn(ItemRestController.class).getSingleItem(item.getUid())).withRel(item.getName()));

            }
        }
        return customers;
    }

    @GetMapping(value = "/{id}")
    public Customer getSingleCustomer(@PathVariable("id") int id){
        Customer customer = customerRepository.findOne(id);
        customer.add(linkTo(methodOn(CustomerRestController.class).getSingleCustomer(id)).withSelfRel());
        if(customer.getCart().getItems().size() > 0){
            customer.add(linkTo(methodOn(CustomerRestController.class).getCustomerCart(customer.getUid())).withRel("cart"));
        }
        return customerRepository.findOne(id);

    }

    @GetMapping(value = "/{id}/cart")
    public Cart getCustomerCart(@PathVariable("id") int id){
        Cart cart =  customerRepository.getOne(id).getCart();
        cart.add(linkTo(methodOn(CustomerRestController.class).getCustomerCart(cart.getUid())).withRel("cart"));
        for(Item item : cart.getItems()){
            item.add(linkTo(methodOn(ItemRestController.class).getSingleItem(item.getUid())).withRel(item.getName()));

        }
        return cart;
    }

}



