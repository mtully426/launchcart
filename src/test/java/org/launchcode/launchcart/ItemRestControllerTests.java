package org.launchcode.launchcart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by LaunchCode
 */
@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ItemRestControllerTests extends AbstractBaseRestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ItemRepository itemRepository;

    @Before
    public void createTestItems() {
        Item item1 = new Item("Test Item 1", 1);
        itemRepository.save(item1);
        Item item2 = new Item("Test Item 2", 2);
        itemRepository.save(item2);
    }

    @Test
    public void testGetAllItems() throws Exception {
        List<Item> items = itemRepository.findAll();
        ResultActions res = mockMvc.perform(get("/api/items"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(items.size())));
        for (int i=0; i < items.size(); i++) {
            res.andExpect(jsonPath("$["+i+"].uid", is(items.get(i).getUid())));
            res.andExpect(jsonPath("$["+i+"].links..href").value("http://localhost/api/items/"+(i+1)));
        }
    }

    @Test
    public void testGetSingleItem() throws Exception {
        Item anItem = itemRepository.findAll().get(0);
        mockMvc.perform(get("/api/items/{id}", anItem.getUid()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("uid").value(anItem.getUid()))
                .andExpect((jsonPath("$.links..href").value("http://localhost/api/items/1")));
    }

    @Test
    public void testPostItem() throws Exception {
        String json = json(new Item("New Item", 42));
        mockMvc.perform(post("/api/items/")
                .content(json)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void testPutItem() throws Exception {
        Item anItem = itemRepository.findAll().get(0);
        String newName = "New item name";
        anItem.setName(newName);
        String json = json(anItem);
        mockMvc.perform(put("/api/items/{id}", anItem.getUid())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("name").value(newName));
    }

    @Test
    public void testPutNotFoundItem() throws Exception {
        Item anItem = itemRepository.findAll().get(0);
        String json = json(anItem);
        mockMvc.perform(put("/api/items/{id}", -1)
                .content(json)
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteItem() throws Exception {
        Item anItem = itemRepository.findAll().get(0);
        int uid = anItem.getUid();
        mockMvc.perform(delete("/api/items/{id}", uid)
                .contentType(contentType))
                .andExpect(status().isOk());
        assertNull(itemRepository.findOne(uid));
    }

    @Test
    public void testDeleteNotFoundItem() throws Exception {
        mockMvc.perform(delete("/api/items/{id}", -1)
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testFilterItemsByPrice() throws Exception {
        itemRepository.save(new Item("42 Item 1", 42));
        itemRepository.save(new Item("42 Item 2", 42));
        mockMvc.perform(get("/api/items?price={price}", 42))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].price", is(42.0)))
                .andExpect(jsonPath("$[1].price", is(42.0)));
    }

    @Test
    public void testFilterItemsByNewItem() throws Exception {
        itemRepository.save(new Item("42 Item 1", 42, "", true));
        itemRepository.save(new Item("41 Item 2", 41, "", false));
        mockMvc.perform(get("/api/items?newItem={newItem}", true))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].newItem", is(true)));

    }

    @Test
    public void testFilterItemsByPriceAndNewItem() throws Exception{
        itemRepository.save(new Item("42 Item 1", 42, "", true));
        itemRepository.save(new Item("41 Item 2", 42, "", false));
        itemRepository.save(new Item("41 Item 3", 41, "", true));
        mockMvc.perform(get("/api/items?newItem={newItem}&price={price}", true, 42))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].newItem", is(true)))
                .andExpect(jsonPath("$[0].price", is(42.0)));

    }

}
