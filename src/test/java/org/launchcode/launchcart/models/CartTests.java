package org.launchcode.launchcart.models;

import org.junit.Test;
import static org.junit.Assert.*;

public class CartTests {

    @Test
    public void testComputerPrice(){
        Item item1 = new Item("Test Item 1", 5);
        Item item2 = new Item("Test Item 2", 5);
        Item item3 = new Item("Test Item 3", 8);
        Item item4 = new Item("Test Item 4", 5);
        Cart cart = new Cart ();
        cart.addItem(item1);
        cart.addItem(item2);
        cart.addItem(item3);
        cart.addItem(item4);
        assertEquals(23, cart.computeTotal(), 0);
    }

    @Test
    public void testRemoveItem(){
        Item item1 = new Item("Test Item 1", 5);
        Item item2 = new Item("Test Item 2", 5);
        Item item3 = new Item("Test Item 3", 8);
        Item item4 = new Item("Test Item 4", 5);
        Cart cart = new Cart ();
        cart.addItem(item1);
        cart.addItem(item2);
        cart.addItem(item3);
        cart.addItem(item4);
        assertEquals(4, cart.getItems().size());
        cart.removeItem(item3);
        assertEquals(3, cart.getItems().size());
    }
}
