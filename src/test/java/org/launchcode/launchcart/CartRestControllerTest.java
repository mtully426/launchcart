package org.launchcode.launchcart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.data.CartRepository;
import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Customer;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@IntegrationTestConfig
@RunWith(SpringRunner.class)
public class CartRestControllerTest extends AbstractBaseRestIntegrationTest{

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void createTestItems(){
       // Customer customer1 = new Customer("Mike1", "Martin");
        //customer1.setCart(new Cart());
        Cart cart1 = new Cart();
        cart1.addItem(new Item("thing 1", 25));
        cartRepository.save(cart1);
        Cart cart2 = new Cart();
        cart2.addItem(new Item("thing 2", 25));
        cartRepository.save(cart2);
    }

    @Test
    public void testGetAllCarts() throws Exception {
        List<Cart> carts = cartRepository.findAll();
        ResultActions res = mockMvc.perform(get("/api/carts"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(carts.size())));
        for (int i=0; i < carts.size(); i++) {
            res.andExpect(jsonPath("$["+i+"].uid", is(carts.get(i).getUid())));
        }
    }

    @Test
    public void testGetSingleCart() throws Exception{
        Cart aCart = cartRepository.findOne(1);
        System.out.println(aCart.getUid());
        mockMvc.perform(get("/api/carts/{id}", aCart.getUid()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("uid").value(aCart.getUid()))
                .andExpect(jsonPath("links..href").value("http://localhost/api/carts/1"));

    }

    @Test
    public void testPutItemInCart() throws Exception{
        Cart aCart = cartRepository.findAll().get(0);
        Item newItem = new Item("Thing 3", 35);
        aCart.addItem(newItem);
        cartRepository.save(aCart);
        String json = json(aCart);;
        mockMvc.perform(put("/api/carts/{id}", aCart.getUid())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("items[1].name").value(aCart.getItems().get(1).getName()));


    }
}
