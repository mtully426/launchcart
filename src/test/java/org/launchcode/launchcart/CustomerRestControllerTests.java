package org.launchcode.launchcart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Customer;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CustomerRestControllerTests extends AbstractBaseRestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CustomerRepository customerRepository;

    @Before
    public void createTestItems() {
        Customer customer1 = new Customer("Mike1", "martin");
        customerRepository.save(customer1);
        Customer customer2 = new Customer("Dave2", "Thomas");
        customerRepository.save(customer2);

    }

    @Test
    public void testGetAllCustomers() throws Exception{
        List<Customer> customers = customerRepository.findAll();
        ResultActions res = mockMvc.perform(get("/api/customers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(customers.size())));
        for (int i=0; i < customers.size(); i++) {
            res.andExpect(jsonPath("$["+i+"].uid", is(customers.get(i).getUid())));
        }
    }

    @Test
    public void testGetCustomerById() throws Exception{
        Customer aCustomer = customerRepository.findAll().get(0);
        aCustomer.setCart(new Cart());
        aCustomer.getCart().addItem(new Item("thing 1", 25));
        mockMvc.perform((get("/api/customers/{id}", aCustomer.getUid())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("uid").value(aCustomer.getUid()))
                .andExpect(jsonPath("username").value(aCustomer.getUsername()))
                .andExpect(jsonPath("links").value("Yo adrian!"));

    }

    @Test
    public void testGetCustomerCart() throws Exception{
        Customer aCustomer = customerRepository.findAll().get(0);
        aCustomer.setCart(new Cart());
        aCustomer.getCart().addItem(new Item("thing 1", 25));
        customerRepository.save(aCustomer);

        mockMvc.perform(get("/api/customers/{id}/cart", aCustomer.getUid()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("items[0].name").value(aCustomer.getCart().getItems().get(0).getName()))
                .andExpect(jsonPath("links..href").value("http://localhost/api/customers//cart"));

    }


}
