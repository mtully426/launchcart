package org.launchcode.launchcart;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.controllers.AuthenticationController;
import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
<<<<<<< HEAD
import static org.hamcrest.core.IsNot.not;
=======
import static org.junit.Assert.assertFalse;
>>>>>>> add-swagger
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by LaunchCode
 */
@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CartControllerTests extends AbstractBaseCustomerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ItemRepository itemRepository;

    @Test
    public void testCanSaveItemToCartAndRedirect() throws Exception {
        Item item = new Item("Test Item", 5);
        itemRepository.save(item);
        mockMvc.perform(post("/addItemsToCart")
                .sessionAttr(AuthenticationController.customerSessionKey, testUserUid)
                .param("ids", Integer.toString(item.getUid())))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/viewCart"));
        mockMvc.perform(get("/viewCart")
                .sessionAttr(AuthenticationController.customerSessionKey, testUserUid))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(item.getName())));
    }

    @Test
<<<<<<< HEAD
    public void testCanComputeCartTotal() throws Exception{
        Item item1 = new Item("Test Item 1", 5);
        Item item2 = new Item("Test Item 2", 5);
        Item item3 = new Item("Test Item 3", 5);
        Item item4 = new Item("Test Item 4", 5);
        Cart cart = new Cart ();
        cart.addItem(item1);
        cart.addItem(item2);
        cart.addItem(item3);
        cart.addItem(item4);
        cartRepository.save(cart);
        mockMvc.perform(get("/cart"))
                .andExpect(content().string(containsString("20")));
    }

    @Test
    public void testCanRemoveFromCart() throws Exception{
        Item item1 = new Item("Test Item 1", 5);
        Item item2 = new Item("Bad Item", 5);
        Item item3 = new Item("Test Item 3", 5);
        Item item4 = new Item("Test Item 4", 5);
        Cart cart = new Cart ();
        cart.addItem(item1);
        cart.addItem(item2);
        cart.addItem(item3);
        cart.addItem(item4);
        cartRepository.save(cart);
        mockMvc.perform(post("/cart/remove-item")
                .param("ids", Integer.toString(item2.getUid())));
        mockMvc.perform(get("/cart"))
                .andExpect(content().string(not(containsString("Bad Item"))));
=======
    public void testTotalDisplayedOnCartView() throws Exception {
        Cart cart = testCustomer.getCart();
        itemRepository.save(new Item("Test Item 1", 1.234));
        itemRepository.save(new Item("Test Item 2", 2.345));
        itemRepository.save(new Item("Test Item 3", 3.456));
        double total = cart.computeTotal();
        mockMvc.perform(get("/viewCart")
                .sessionAttr(AuthenticationController.customerSessionKey, testUserUid))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(Double.toString(total))));
    }

    @Test
    public void testCanRemoveItemFromCart() throws Exception {
        Cart cart = testCustomer.getCart();
        Item item1 = new Item("Test Item 1", 1.234);
        itemRepository.save(item1);
        cart.addItem(item1);
        mockMvc.perform(post("/removeItemsFromCart")
                .sessionAttr(AuthenticationController.customerSessionKey, testUserUid)
                .param("ids", Integer.toString(item1.getUid())))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/viewCart"));
        assertFalse(cart.getItems().contains(item1));
>>>>>>> add-swagger
    }

}
